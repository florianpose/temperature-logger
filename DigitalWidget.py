# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Digital Display Widget
#
# Copyright (C) 2018 Florian Pose
#
# ----------------------------------------------------------------------------

from PyQt5.QtWidgets import QFrame
from PyQt5.QtGui import QPainter
from PyQt5.QtCore import Qt, QLocale


# ----------------------------------------------------------------------------

class DigitalWidget(QFrame):

    def __init__(self, parent):
        super(DigitalWidget, self).__init__(parent)
        self.value = None

    def setValue(self, value):
        self.value = value
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        loc = QLocale()
        if self.value is None:
            txt = '--,-'
        else:
            txt = loc.toString(self.value, 'f', 1)
        txt += ' °C'

        rect = self.contentsRect()
        painter.drawText(rect, Qt.AlignCenter, txt)

# ----------------------------------------------------------------------------
