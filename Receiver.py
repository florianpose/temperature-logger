# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Data Receiver
#
# Copyright (C) 2018 Florian Pose
#
# ----------------------------------------------------------------------------

import serial

from PyQt5 import QtCore


# ----------------------------------------------------------------------------

class Receiver(QtCore.QObject):

    receivedValue = QtCore.pyqtSignal(int, bool, float)
    finished = QtCore.pyqtSignal()

    def __init__(self, logger):
        super(Receiver, self).__init__()
        self.logger = logger

    def start(self):
        self.logger.info('Receiver thread started.')

        try:
            ser = serial.Serial(
                port='/dev/ttyUSB0',  # change to COMx on Windows
                baudrate=9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=None)
        except Exception:
            self.logger.error('Failed to open port!', exc_info=True)
            self.finished.emit()
            return

        self.logger.info('Connected to %s', ser.portstr)

        run = True
        data = b''
        bytesReceived = 0

        while run:
            c = ser.read()
            bytesReceived += 1
            if c == b'\r':
                string = data.decode('latin1')
                data = b''

                # self.logger.info('data (%i) %s', len(string), repr(string))
                # 41010100001125 = ch. 1, 112.5 °C
                if len(string) == 15:
                    try:
                        channel = int(string[2])
                    except Exception:
                        continue

                    try:
                        value = int(string[7:15]) * 0.1
                        valid = True
                    except Exception:
                        value = 0.0
                        valid = False

                    self.receivedValue.emit(channel, valid, value)
            else:
                data += c

        ser.close()
        self.logger.info('Receiver thread finished.')
        self.finished.emit()

# ----------------------------------------------------------------------------
