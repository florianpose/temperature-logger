# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Graph Display Widget
#
# Copyright (C) 2018 Florian Pose
#
# ----------------------------------------------------------------------------

from PyQt5.QtWidgets import QFrame
from PyQt5.QtGui import QPainter, QFontMetrics, QColor, QPolygon
from PyQt5.QtCore import Qt, QPoint, QRect, QRectF, QLocale, \
    QDateTime, QLineF


# ----------------------------------------------------------------------------

class GraphWidget(QFrame):

    def __init__(self, parent, logger):
        super(GraphWidget, self).__init__(parent)
        self.value = None
        self.logger = logger
        self.tempLimits = (15.0, 800.0)
        self.tempTics = [15.0, 100.0, 200.0, 300.0, 400.0, 500.0, 600.0,
                         700.0, 800.0]
        self.timeRange = 600.0
        self.minTics = range(0, 11)
        self.colors = []

        self.channelData = []
        for i in range(0, 4):
            self.channelData.append([])
            self.colors.append(QColor(0, 0, 0))

    def setValue(self, channel, value):
        if channel < 1 or channel > 4:
            return

        now = QDateTime.currentDateTime()
        data = self.channelData[channel - 1]
        data.append((now, value))
        self.removeOld(now)
        self.update()

    def clearValues(self):
        for i in range(0, 4):
            self.channelData[i] = []
        self.update()

    def setColor(self, idx, color):
        if idx < 0 or idx >= 4:
            return

        self.colors[idx] = color
        self.update()

    def removeOld(self, now):
        limit = now.addSecs(-self.timeRange)
        for i in range(0, 4):
            data = self.channelData[i]
            if not data:
                continue
            while len(self.channelData[i]) > 0:
                (time, value) = self.channelData[i][0]
                if time >= limit:
                    break
                self.channelData[i] = self.channelData[i][1:]

    def paintEvent(self, event):
        super(GraphWidget, self).paintEvent(event)

        painter = QPainter(self)
        rect = self.contentsRect()

        # scale

        fm = QFontMetrics(painter.font())
        timeFontRect = fm.boundingRect('0 Min.')
        timeScaleRect = QRect(rect)
        timeScaleRect.setTop(rect.bottom() - timeFontRect.height())

        tempFontRect = fm.boundingRect('800 °C')
        tempScaleRect = QRect(rect)
        tempScaleRect.setWidth(tempFontRect.width())

        dataRect = QRect(rect)
        dataRect.setBottom(timeScaleRect.top() - 1)
        dataRect.setLeft(tempScaleRect.right() + 4)

        tempDiff = self.tempLimits[1] - self.tempLimits[0]
        tempScale = dataRect.height() / tempDiff

        timeScale = dataRect.width() / self.timeRange

        painter.save()
        pen = painter.pen()
        pen.setColor(QColor(200, 200, 200))
        pen.setWidthF(1.0)
        pen.setStyle(Qt.DashLine)
        painter.setPen(pen)

        loc = QLocale()

        for tempTic in self.tempTics:
            yPix = (tempTic - self.tempLimits[0]) * tempScale
            y = dataRect.bottom() - yPix
            txt = loc.toString(tempTic) + ' °C'
            line = QLineF(rect.left(), y, rect.right(), y)
            painter.drawLine(line)
            h = tempFontRect.height()
            textRect = QRectF(rect.left() + 2, y - h, rect.width() - 2, h)
            # painter.fillRect(textRect, Qt.red)
            # self.logger.info('%lf %s', tempTic, textRect)
            painter.drawText(textRect, Qt.AlignLeft | Qt.AlignBottom, txt)

        for minTic in self.minTics:
            txt = loc.toString(minTic) + ' Min.'
            dt = minTic * 60.0
            x = dataRect.left() + dt * timeScale
            line = QLineF(x, rect.top(), x, rect.bottom())
            painter.drawLine(line)
            h = timeFontRect.height()
            x += 2
            textRect = QRectF(x, timeScaleRect.top(),
                              timeScaleRect.width() - x, h)
            # painter.fillRect(textRect, Qt.red)
            # self.logger.info('%lf %s', tempTic, textRect)
            painter.drawText(textRect, Qt.AlignLeft | Qt.AlignTop, txt)

        painter.restore()

        # data

        minTime = QDateTime.currentDateTime()
        for data in self.channelData:
            if len(data):
                firstTime = data[0][0]
                if firstTime < minTime:
                    minTime = firstTime

        idx = -1
        for data in self.channelData:
            idx += 1
            poly = QPolygon()
            for time, value in data:
                if value is None:
                    if poly:
                        self.drawPoly(painter, idx, poly)
                    poly = QPolygon()
                else:
                    dt = minTime.msecsTo(time) * 1e-3
                    x = dataRect.left() + dt * timeScale
                    yPix = (value - self.tempLimits[0]) * tempScale
                    y = dataRect.bottom() - yPix
                    poly.append(QPoint(x, y))
            if poly:
                self.drawPoly(painter, idx, poly)

    def drawPoly(self, painter, idx, poly):
        pen = painter.pen()
        pen.setColor(self.colors[idx])
        pen.setWidthF(1.5)
        painter.setPen(pen)
        painter.drawPolyline(poly)

# ----------------------------------------------------------------------------
