# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Main Widget
#
# Copyright (C) 2018 Florian Pose
#
# ----------------------------------------------------------------------------

from PyQt5.QtGui import QColor, QIcon
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget, QAction
from PyQt5.QtCore import Qt, QThread

from DigitalWidget import DigitalWidget
from GraphWidget import GraphWidget
from Receiver import Receiver


# ----------------------------------------------------------------------------

class MainWidget(QWidget):

    colors = [
        QColor(38, 139, 210),   # blue
        QColor(220,  50,  47),  # red
        QColor(133, 153,   0),  # green
        QColor(181, 137,   0),  # yellow
        QColor(211,  54, 130),  # magenta
        QColor(108, 113, 196),  # violet (close to blue)
        QColor(42, 161, 152),   # cyan
        QColor(203,  75,  22),  # orange (close to red)
        ]

    def __init__(self, config, logger):
        super(MainWidget, self).__init__()

        self.config = config
        self.logger = logger

        # Shortcut -----------------------------------------------------------

        self.resetAction = QAction(QIcon(), "Reset", self)
        self.resetAction.setShortcut("R")
        self.resetAction.setShortcutContext(Qt.ApplicationShortcut)
        self.addAction(self.resetAction)

        self.resetAction.triggered.connect(self.reset)

        # Appearance ---------------------------------------------------------

        self.resize(800, 600)

        self.setWindowTitle('Temperatur-Logger')

        self.setStyleSheet("""
            background-color: rgb(0, 34, 44);
            """)

        # Sub-widgets --------------------------------------------------------

        layout = QHBoxLayout(self)

        self.graph = GraphWidget(self, self.logger)
        self.graph.setStyleSheet("""
            font-size: 15px;
            """)
        for idx in range(0, 4):
            self.graph.setColor(idx, self.colors[idx])
        layout.addWidget(self.graph, 4)

        sideLayout = QVBoxLayout()
        layout.addLayout(sideLayout, 1)

        self.digitals = []
        for i in range(0, 4):
            dig = DigitalWidget(self)
            style = """
                font-size: 40px;
                background-color: rgb(0, 44, 54);
                """
            style += 'color: ' + self.colors[i].name()
            dig.setStyleSheet(style)

            sideLayout.addWidget(dig, 0)
            self.digitals.append(dig)

        self.receiverThread = QThread()
        self.receiver = Receiver(self.logger)
        self.receiver.receivedValue.connect(self.receivedValue)
        self.receiver.moveToThread(self.receiverThread)
        self.receiverThread.started.connect(self.receiver.start)
        self.receiverThread.start()

        self.logger.info('Setup finished.')

    def receivedValue(self, channel, valid, value):
        # self.logger.info('ch=%i val=%.1lf', channel, value)
        if channel > 0 and channel <= 4:
            dig = self.digitals[channel - 1]
            if valid:
                valNone = value
            else:
                valNone = None

            dig.setValue(valNone)
            self.graph.setValue(channel, valNone)

    def reset(self):
        self.graph.clearValues()

# ----------------------------------------------------------------------------
