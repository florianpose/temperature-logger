# Grafischer Temperaturlogger

Lizenz: GPLv3
Author: Florian Pose <florian@pose.nrw>

## Abhängigkeiten unter Windows installieren:

- PowerShell öffnen

```msshell
python
```

Ein Dialog öffnet sich. Frage, ob Python (z. B. Version 3.10) installiert
werden soll, bestätigen. Danach noch die benötigten Bibliotheken installieren:

```msshell
pip install PyQt5 pyserial
```

## Konfiguration

- in Datei Receiver.py (~ Zeile 31) Adresse des COM-Ports eintragen
